﻿define([
        "dojo/_base/array",
        "dojo/_base/connect",
        "dojo/_base/declare",
        "dojo/_base/lang",
        "dijit/focus",

        "dijit/_CssStateMixin",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dijit/_WidgetsInTemplateMixin",
        "epi-cms/widget/_HasChildDialogMixin",

        "dojo/on",
        "dojo/dom",
        "dijit/TooltipDialog",
        "dijit/popup",


        "epi/epi",
        "epi/shell/widget/_ValueRequiredMixin",
        'fotoware-image/ImageModel',
        'dojo/text!./imageSelector.html',

        'xstyle/css!./imageSelector.css'
],
    function (
        array,
        connect,
        declare,
        lang,
        focusManager,

        _CssStateMixin,
        _Widget,
        _TemplatedMixin,
        _WidgetsInTemplateMixin,
        _HasChildDialogMixin,

        on,
        dom,
        TooltipDialog,
        popup,

        epi,
        _ValueRequiredMixin,
        ImageModel,
        template

    ) {

        return declare("fotoware-image.Widgets.imageSelector", [_Widget, _TemplatedMixin, _WidgetsInTemplateMixin, _CssStateMixin, _ValueRequiredMixin, _HasChildDialogMixin], {
            postMixInProperties: function() {
                this.inherited(arguments);
            },

            newWindow: null,

            templateString: template,

            baseClass: "imageExt",

            helptext: "Place items on separate lines",

            value: null,

            containerId: "Fotoware_ImageProperty_Frame_Container",
            iframeId: "Fotoware_ImageProperty_Frame",
            closeBtnId: "Fotoware_ImageProperty_Frame_Container_Close",
            innerClass: "Fotoware_ImageProperty_Frame_Wraper",
            browserClass: "Fotoware_Browser",
            publisherClass: "Fotoware_Publiher",

            postCreate: function () {

                this.inherited(arguments);
            },
            //Preapre Image browsing frame
            loadFrame: function () {

                var root = document.body;
                var div = document.createElement("div");
                div.setAttribute("id", this.containerId);
                div.setAttribute("class", this.containerId);
               
                var innerContainer = document.createElement("div");
                innerContainer.setAttribute("class", this.innerClass +" "+ this.browserClass);

                var ifrm = document.createElement("IFRAME");
                ifrm.setAttribute("src", this.fotwareUrl + this.browseUrl);
                ifrm.setAttribute("id", this.iframeId);
         

                var closeBtn = document.createElement("a");
                closeBtn.setAttribute("class", this.closeBtnId);
                closeBtn.setAttribute("id", this.closeBtnId);
                closeBtn.setAttribute("title", "Close window");
             
                closeBtn.onclick =  lang.hitch(this,function () {
                    this.closeWindow();
                });

                innerContainer.appendChild(ifrm);
                innerContainer.appendChild(closeBtn);
                div.appendChild(innerContainer);
                root.appendChild(div);
            },
            //Remove frame from dom
            unloadFrame: function () {
                var root = document.getElementById(this.containerId);
                root.parentNode.removeChild(root);
            },
            //Handle the frame events
            reciveMessageHandler: function (event) {

                if (event.data.event == "assetSelected") {

                    var node = event.data.asset.href;
                    var iframe = document.getElementById(this.iframeId);
                    iframe.parentNode.setAttribute("class", this.innerClass + " " + this.publisherClass);
                    iframe.setAttribute("src", this.buildUrl(node));

                } else if (event.data.event == "assetExported") {
                    this.setImageValue(event.data.export);
                    this.set("value", this.currentImage);
                    this.closeWindow();
                    this.onChange(this.currentImage);
                }
            },

            //build the publishing url for the image
            buildUrl: function ( node) {

                var baseUrl = this.fotwareUrl + this.publishUrl +'?i=' + node;
               
                baseUrl = baseUrl + this.getUrlPart('&w=', this.defaultWidth);
                baseUrl = baseUrl + this.getUrlPart('&h=', this.defaultHeight);
                baseUrl = baseUrl + this.getUrlPart('&p=', this.defaultPreset);

                return baseUrl;
            },

            getUrlPart: function(key, value) {
                if (typeof(value) != 'undefined' && value != "") {
                    return  key +value;
                }
                return "";
            },

            //set image object proeperties from json model
            setImageValue: function (data) {

                if (this.currentImage == null)
                    this.currentImage = new ImageModel(null);

                this.currentImage.url = data.export.image.normal;
                this.currentImage.doubleSizeUrl = data.export.image.doubleResolution;
                this.currentImage.thumbUrl = data.export.image.preview;
                this.currentImage.width = data.export.size.w;
                this.currentImage.height = data.export.size.h;
                this.currentImage.metas.caption = data.caption.label;
                this.currentImage.metas.tooltip = data.caption.tooltip;
                this.currentImage.metas.publication = data.publication.text;
                this.currentImage.metas.metaData = data.metadata;
            },

            //add events of propert set vale
            _setValueAttr: function (value) {

                this.loadThumb(value);
                this.AddButtonShow(value);
                this._set("value", value);
            },

            AddButtonShow: function (value) {

                if (value == null) {
                    this.noImageNode.style.display = "block";
                } else {
                    this.noImageNode.style.display = "none";
                }
            },

            openWindow: function (evt) {

                this.onFocus();
                this.isShowingChildDialog = true;
                this.loadFrame();
                var callback = lang.hitch(this, this.reciveMessageHandler);
                this.messageReceivedCallback = callback;
                window.addEventListener("message", callback, false);
            },

            loadThumb: function (value) {

                if (value == null) {
                    this.thumbContainer.style.display = "none";
                    this.thumbImg.src = "";
                } else {
                    this.thumbContainer.style.display = "block";
                    this.thumbImg.src = value.thumbUrl;

                    this.buildToolTip(value);

                }
            },
            
            //build image tooltip
            buildToolTip : function(object) {
                
                var createHtml = function () {
                    return "<div>CAPTION: <br />\"" + object.metas.caption + "\" <br />TOOLTIP:<br /> " + object.metas.tooltip + "<br/> META CODES: <br/> " + JSON.stringify(object.metas.metaData) + " </div>";
                };


                var myTooltipDialog = new TooltipDialog({
                    connectId: [this.thumbContainer.id],
                    content: createHtml(),
                    onMouseLeave: function () {
                        popup.close(myTooltipDialog);
                    }
                });

                on(this.thumbContainer, 'mouseover', dojo.hitch(this,  function () {
                    popup.open({
                        popup: myTooltipDialog,
                        around: this.thumbContainer
                    });
                }));

                on(this.thumbContainer, 'mouseleave',dojo.hitch(this,  function () {
                    popup.close(myTooltipDialog);
                }));

            },

            //Delete image event
            clearProperty: function (evt) {

                this.set("value", null);
                this.onChange(null);
                this.onFocus();
            },

            //handle close window event
            closeWindow: function (evt) {

                window.removeEventListener('message', this.messageReceivedCallback);
                this.messageReceivedCallback = null;
                this.isShowingChildDialog = false;
                this.unloadFrame();
            },

            messageReceivedCallback: null
        });
    });