﻿namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// 
    /// </summary>
    public interface IImageStorage
    {
        string SaveToStorage(string imageUrl, string storageContainerIdentyficator, string name);
        string GetUrl(string idInStorage);
    }
}