﻿using System;
using System.IO;
using System.Text;
using EPiServer.Core;
using EPiServer.Framework.Serialization;
using EPiServer.PlugIn;
using EPiServer.ServiceLocation;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Fotoware property backing type allows to serialize object to Episervere data and deserialize content for property use. Default implamentaion of episerver property data
    /// </summary>
    [PropertyDefinitionTypePlugIn(DisplayName = "Fotware Image")]
    public class FotowarePropertyBackingType : PropertyData
    {
        private readonly IObjectSerializer _serializer;
        private FotowareImage value;

        public FotowarePropertyBackingType()
        {
            _serializer = GetSerializer();
        }

        public FotowarePropertyBackingType(FotowareImage image)
        {
            _serializer = GetSerializer();
            SetValue(image);
        }

        public override object Value
        {
            get { return GetValue(); }
            set { SetValue((FotowareImage) value); }
        }


        public override Type PropertyValueType
        {
            get { return typeof (FotowareImage); }
        }

        public override PropertyDataType Type
        {
            get { return PropertyDataType.LongString; }
        }

        protected FotowareImage DefaultValue
        {
            get { return null; }
        }

        /// <summary>
        /// Get default IObjectSerializer JSON serializer
        /// </summary>
        /// <returns></returns>
        private static IObjectSerializer GetSerializer()
        {
            var objectSerializerFactory = ServiceLocator.Current.GetInstance<IObjectSerializerFactory>();
            IObjectSerializer objectSerializer = objectSerializerFactory.GetSerializer(KnownContentTypes.Json);
            return objectSerializer;
        }

        public FotowareImage GetValue()
        {
            return value;
        }

        public void SetValue(FotowareImage value)
        {
            SetPropertyValue(value, delegate
            {
                if (Equals(value, DefaultValue))
                {
                    Clear();
                }
                else
                {
                    this.value = value;
                    Modified();
                }
            });
        }

        protected override void SetDefaultValue()
        {
            ThrowIfReadOnly();
            value = DefaultValue;
        }

        public override PropertyData ParseToObject(string value)
        {
            return new FotowarePropertyBackingType(DeserializeValue(value));
        }

        public override void ParseToSelf(string value)
        {
            SetValue(DeserializeValue(value));
        }

        public override void LoadData(object objValue)
        {
            if (objValue is FotowareImage)
                SetValue((FotowareImage) objValue);
            else
                SetValue(DeserializeValue((string) objValue));
        }

        public override object SaveData(PropertyDataCollection properties)
        {
            return SerializeValue(GetValue());
        }

        private FotowareImage DeserializeValue(string stringValue)
        {
            try
            {
                return _serializer.Deserialize<FotowareImage>(new StringReader(stringValue));
            }
            catch (Exception e)
            {
                throw new Exception(
                    "Failed to deserialize property imageValue." + stringValue, e);
            }
        }

        /// <summary>
        /// Serializes the value.
        /// </summary>
        /// <param name="imageValue">Fotoware object</param>
        /// <returns></returns>
        private string SerializeValue(FotowareImage imageValue)
        {
            var result = new StringBuilder();
            _serializer.Serialize(new StringWriter(result), imageValue);
            return result.ToString();
        }
    }
}