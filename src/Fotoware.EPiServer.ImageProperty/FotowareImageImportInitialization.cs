﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using EPiServer;
using EPiServer.Core;
using EPiServer.Framework;
using EPiServer.Framework.Initialization;
using EPiServer.ServiceLocation;
using EPiServer.Shell.ObjectEditing;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Initialize store saveing to handle image upload on page save
    /// </summary>
    [ModuleDependency(typeof (ServiceContainerInitialization))]
    [InitializableModule]
    public class FotowareImageImportInitialization : IConfigurableModule
    {
        /// <summary>
        /// Register the default Image storage in default episerver IOC
        /// </summary>
        /// <param name="context">The context.</param>
        public void ConfigureContainer(ServiceConfigurationContext context)
        {
            context.Container.Configure(exp => exp.For<IImageStorage>().HttpContextScoped().Use<LocalPageImageStorage>());
        }

        public void Initialize(InitializationEngine context)
        {
            DataFactory.Instance.SavingPage += OnSavingPage;
        }

        public void Uninitialize(InitializationEngine context)
        {
            DataFactory.Instance.SavingPage -= OnSavingPage;
        }

        public void Preload(string[] parameters)
        {
        }

        private void OnSavingPage(object sender, PageEventArgs e)
        {
            if (e.Page == null)
            {
                return;
            }
            CheckPageForImageToUpload(e.Page);
        }

        /// <summary>
        /// Find fotoware property on page data and handle the upload
        /// </summary>
        /// <param name="page">The page.</param>
        private void CheckPageForImageToUpload(PageData page)
        {
            foreach (PropertyData property in RetriveFotowareProperties(page))
            {
                if (!property.IsModified)
                    continue;

                FotowareConfigurationAttribute attributeConfiguration = GetPropertyConfigurationThroughReflection(page,
                    property);

                if (attributeConfiguration == null || !attributeConfiguration.StoreFile)
                    continue;

                HandleImageUpload(page, property);
            }
        }

        /// <summary>
        /// Handles the image upload.
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="property">The property.</param>
        private void HandleImageUpload(PageData page, PropertyData property)
        {
            var fotowareImage = (FotowareImage) property.Value;
            if (fotowareImage == null)
                return;

            string content = UploadImage(fotowareImage.DoubleSizeUrl, page);
            if (string.IsNullOrEmpty(content))
                return;

            UpdateProperty(page, property, fotowareImage, content);
        }

        /// <summary>
        /// Uploads the image into the storage
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="page">The page.</param>
        /// <returns></returns>
        private string UploadImage(string url, PageData page)
        {
            var imageStorage = ServiceLocator.Current.GetInstance<IImageStorage>();
            return imageStorage.SaveToStorage(url, page.PageLink.ID.ToString(), string.Empty);
        }

        /// <summary>
        /// Updates the property
        /// </summary>
        /// <param name="page">The page.</param>
        /// <param name="property">The property.</param>
        /// <param name="fotowareImage">The fotoware image.</param>
        /// <param name="content">The content.</param>
        private void UpdateProperty(PageData page, PropertyData property, FotowareImage fotowareImage, string content)
        {
            fotowareImage.StorageId = content;
            page.SetValue(property.Name, fotowareImage);
        }


        /// <summary>
        /// Retrives the fotoware properties from page data.
        /// </summary>
        /// <param name="pageData">The page data.</param>
        /// <returns></returns>
        private IEnumerable<PropertyData> RetriveFotowareProperties(PageData pageData)
        {
            return pageData.Property.Where(p => p.GetType() == typeof (FotowarePropertyBackingType));
        }

        /// <summary>
        /// Gets the property configuration FotowareConfigurationAttribute through reflection.
        /// </summary>
        /// <param name="page">Page data</param>
        /// <param name="property">Property to get config from.</param>
        /// <returns></returns>
        private FotowareConfigurationAttribute GetPropertyConfigurationThroughReflection(PageData page,
            PropertyData property)
        {
            PropertyInfo proeprtyInfo =
                page.GetOriginalType().GetProperties().FirstOrDefault(p => p.Name == property.Name);

            if (proeprtyInfo == null)
                return null;

            return proeprtyInfo.GetCustomAttributes(true).OfType<FotowareConfigurationAttribute>().FirstOrDefault();
        }
    }
}