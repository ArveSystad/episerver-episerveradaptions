﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using EPiServer.Shell.ObjectEditing;
using EPiServer.Shell.ObjectEditing.EditorDescriptors;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Episerver Editor descriptor for fotoware custom property
    /// </summary>
    [EditorDescriptorRegistration(TargetType = typeof (FotowareImage), EditorDescriptorBehavior = EditorDescriptorBehavior.Default)]
    public class FotowarePropertyDescriptior : EditorDescriptor
    {
       //Default configs value
        private const string DefaultFotowareUrl = "https://beta.fotoware.com";
        private const string DefaultBrowseSufixUrl = "/fotoweb/widgets/selection";
        private const string DefaultPublishSufixUrl = "/fotoweb/widgets/publish";

        public FotowarePropertyDescriptior()
        {
            ClientEditingClass = "fotoware-image.widgets.ImageSelector";
        }

        public override void ModifyMetadata(ExtendedMetadata metadata, IEnumerable<Attribute> attributes)
        {
            Attribute[] att = attributes.ToArray();
            base.ModifyMetadata(metadata, att);

            //set all configuration objects of the property
            string fotwareUrl = GetWebConfigValue("FotowareImageServiceAddress", DefaultFotowareUrl);
            string browseUrl = GetWebConfigValue("FotowareBrowseUrl", DefaultBrowseSufixUrl);
            string publishUrl = GetWebConfigValue("FotowarePublishUrl", DefaultPublishSufixUrl);
            string defaultHeight = GetConfiguration(att, p => p.Height.ToString(), string.Empty);
            string defaultWidth = GetConfiguration(att, p => p.Width.ToString(), string.Empty);
            string defaultPreset = GetConfiguration(att,  p => p.CustomPreset, string.Empty);

            bool storeLocaly = att.OfType<FotowareConfigurationAttribute>().Select(a => a.StoreFile).FirstOrDefault();
           
            var currentImage = ((FotowareImage) ((FotowarePropertyBackingType) metadata.Model).Value);

            //add configuration to proeprty avaiable in js proeprty plugin.
            metadata.EditorConfiguration.Add("fotwareUrl", fotwareUrl);
            metadata.EditorConfiguration.Add("browseUrl", browseUrl);
            metadata.EditorConfiguration.Add("publishUrl", publishUrl);
            metadata.EditorConfiguration.Add("defaultHeight", defaultHeight);
            metadata.EditorConfiguration.Add("defaultWidth", defaultWidth);
            metadata.EditorConfiguration.Add("defaultPreset", defaultPreset);
            metadata.EditorConfiguration.Add("storeLocaly", storeLocaly);
            metadata.EditorConfiguration.Add("currentImage", currentImage);
        }

      
        private string GetConfiguration(IEnumerable<Attribute> att, Func<FotowareConfigurationAttribute, string> selector, string defaultValue)
        {
            return CofigurationRetrive(att, selector) ?? defaultValue;
        }

        public string CofigurationRetrive(IEnumerable<Attribute> att,  Func<FotowareConfigurationAttribute,string> selector )
        {
            return att.OfType<FotowareConfigurationAttribute>().Select(selector).FirstOrDefault();
        }
        private string GetWebConfigValue(string attribute, string defaultValue)
        {
            return ConfigurationManager.AppSettings[attribute] ?? defaultValue;
        }
    }
}