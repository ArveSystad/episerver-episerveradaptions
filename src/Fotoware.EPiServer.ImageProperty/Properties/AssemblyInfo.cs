﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("FotoWeb EPiServer Plugin")]
[assembly: AssemblyDescription("The FotoWeb EPiServer plugin let you insert images from inside FotoWeb to your EPiServer pages.")]


// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6f947aec-b85c-4027-8e54-b13bccd6cc89")]
