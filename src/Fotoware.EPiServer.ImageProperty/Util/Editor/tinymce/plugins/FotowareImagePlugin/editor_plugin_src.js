﻿(function () {

    tinymce.create('tinymce.plugins.FotowareImageButton', {

        init: function (ed, url) {

            var containerId = "Fotoware_ImageProperty_Frame_Container";
            var iframeId = "Fotoware_ImageProperty_Frame";
            var closeBtnId = "Fotoware_ImageProperty_Frame_Container_Close";
            var fotowareConfiguration = ed.getParam('fotoware-service-address');
            var browseUrl = ed.getParam('fotoware-browse-url');
            var publishUrl = ed.getParam('fotoware-publish-url');

            ed.addCommand('fotowareImagePluginCmd', function () {

                function loadFrame() {

                    var root = document.body;

                    var div = document.createElement("div");
                    div.setAttribute("id", containerId);
                    div.setAttribute("style", "display: block; position: fixed; z-index: 9999;width: 100%;height: 100%;text-align: center;top: 0;left: 0;background: black;background: rgba(0,0,0,0.8);padding-top:120px;");

                    var innerContainer = document.createElement("div");
                    innerContainer.setAttribute("style", "width: 45%;height: 55%;margin: 0 auto; position: relative;");

                    var ifrm = document.createElement("IFRAME");
                    ifrm.setAttribute("src", fotowareConfiguration + browseUrl);
                    ifrm.setAttribute("style", "width: 100%;height: 100%;background: white;");
                    ifrm.setAttribute("id", iframeId);

                    var closeBtn = document.createElement("img");
                    closeBtn.setAttribute("src", "/Util/Editor/tinymce/plugins/FotowareImagePlugin/images/close-icon.png");
                    closeBtn.setAttribute("id", closeBtnId);
                    closeBtn.setAttribute("style", " display: block;position: absolute; z-index: 9999;width: 48px;height: 48px; right: -26px;top: -26px;cursor: pointer; ");
                    closeBtn.onclick = function () {
                        unloadFrame();
                    };

                    innerContainer.appendChild(ifrm);
                    innerContainer.appendChild(closeBtn);

                    div.appendChild(innerContainer);
                    root.appendChild(div);
                    window.addEventListener("message", receiveMessage, false);
                }

                function unloadFrame() {
                    var root = document.getElementById(containerId);
                    root.parentNode.removeChild(root);
                    window.removeEventListener('message', receiveMessage);
                }

                loadFrame();
                function receiveMessage(event) {

                    if (event.origin !== fotowareConfiguration)
                        return;

                    if (event.data.event == "assetSelected") {

                        var node = event.data.asset.href;
                        var iframe = document.getElementById(iframeId);
                        iframe.parentNode.setAttribute("style", "width: 75%;height: 75%;margin: 0 auto; position: relative;");
                        iframe.setAttribute("src", fotowareConfiguration + publishUrl+ '?i=' + node );

                    }
                    else if (event.data.event == "assetExported") {

                        unloadFrame();
                        ed.execCommand('mceInsertContent', false, "<img src=\"" + event.data.export.export.image.normal + "\" />");
                    }
                }
            });

            ed.addButton('addFotowareImage', {
                title: 'Add Image',
                cmd: 'fotowareImagePluginCmd',
                image: '/Util/Editor/tinymce/plugins/FotowareImagePlugin/images/fwAdd.png'
            });
        }
    });

    tinymce.PluginManager.add('FotowareImagePlugin', tinymce.plugins.FotowareImageButton);

})();
