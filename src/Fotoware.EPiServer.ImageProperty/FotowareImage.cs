﻿using System;
using System.Collections.Generic;

namespace Fotoware.EPiServer.ImageProperty
{
    [Serializable]
    public class FotowareImageMeta
    {
        public string Caption { get; set; }
        public string Tooltip { get; set; }
        public string Publication { get; set; }
        public Dictionary<string, string> MetaData { get; set; }
    }

    [Serializable]
    public class FotowareImage
    {
        public string StorageId { get; set; }
        public string DoubleSizeUrl { get; set; }
        public string Url { get; set; }
        public string ThumbUrl { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public FotowareImageMeta Metas { get; set; }
    }
}