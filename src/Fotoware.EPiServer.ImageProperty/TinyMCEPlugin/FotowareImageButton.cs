﻿using System.Collections.Generic;
using System.Configuration;
using EPiServer.Editor.TinyMCE;

namespace Fotoware.EPiServer.TinyMCE.TinyMCEPlugin
{
    /// <summary>
    /// Default TinyMCE plugin registration.
    /// </summary>
    [TinyMCEPluginButton(PlugInName = "FotowareImagePlugin", ButtonName = "addFotowareImage", GroupName = "misc",
        IconUrl = "Editor/tinymce/plugins/FotowareImagePlugin/images/fwAdd.png",
        DynamicConfigurationOptionsHandler = typeof (FotowareImageButton))]
    public class FotowareImageButton : IDynamicConfigurationOptions
    {
        public IDictionary<string, object> GetConfigurationOptions()
        {
            var customSettings = new Dictionary<string, object>();
            customSettings.Add("fotoware-service-address",
                ConfigurationManager.AppSettings["FotowareImageServiceAddress"] ?? "https://dev.fotoware.com");
            customSettings.Add("fotoware-browse-url",
                ConfigurationManager.AppSettings["FotowareBrowseUrl"] ?? "/fotoweb/views/selection");
            customSettings.Add("fotoware-publish-url",
                ConfigurationManager.AppSettings["FotowarePublishUrl"] ?? "/fotoweb/views/publish");
            return customSettings;
        }
    }
}
