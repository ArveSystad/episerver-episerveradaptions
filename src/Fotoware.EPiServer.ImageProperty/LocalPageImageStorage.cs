﻿using System;
using System.IO;
using System.Net;
using System.Web;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAccess;
using EPiServer.Framework.Blobs;
using EPiServer.ServiceLocation;
using EPiServer.Web;
using EPiServer.Web.Routing;

namespace Fotoware.EPiServer.ImageProperty
{
    /// <summary>
    /// Default implementation of Image storage
    /// </summary>
    public class LocalPageImageStorage : IImageStorage
    {
        /// <summary>
        /// content repository will be use to store Image as episerver mediaData
        /// </summary>
        private readonly IContentRepository _contentRepository;

        public LocalPageImageStorage()
        {
            _contentRepository = ServiceLocator.Current.GetInstance<IContentRepository>();
        }

        /// <summary>
        /// Saves image as media data into epi storage by given Identification and return ID of the content
        /// </summary>
        /// <param name="imageUrl">The image URL.</param>
        /// <param name="storageContainerIdentyficator">The storage container identyficator in this implementaion ID of the page on which the image is added</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        /// <exception cref="Exception">No current context page to store image</exception>
        public string SaveToStorage(string imageUrl, string storageContainerIdentyficator, string name)
        {
            if (HttpContext.Current == null)
                throw new Exception("No current context page to store image");

            //Prepare the rewuest for image
            var request = (HttpWebRequest) WebRequest.Create(imageUrl);
            var response = (HttpWebResponse) request.GetResponse();


            //Handle the response
            if ((response.StatusCode == HttpStatusCode.OK ||
                 response.StatusCode == HttpStatusCode.Moved ||
                 response.StatusCode == HttpStatusCode.Redirect) &&
                response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {
                //Instantionte helpers for handling the MediaData
                var contentTypeRepository = ServiceLocator.Current.GetInstance<IContentTypeRepository>();
                var mediaDataResolver = ServiceLocator.Current.GetInstance<ContentMediaResolver>();
                var blobFactory = ServiceLocator.Current.GetInstance<BlobFactory>();


                string extension = GetFileExtension(imageUrl);
                object filename = GetFileName(imageUrl);

                //Load default type for file extension
                Type mediaType = mediaDataResolver.GetFirstMatching(extension);
                //load default Epi content type for given media type
                ContentType contentType = contentTypeRepository.Load(mediaType);

                //Get or create the FOR THIS PAGE contentAsset forlder id
                ContentReference currentStorageFolder = GetContainerForStorage(storageContainerIdentyficator);

                //Create a MediaData object
                var media =
                    _contentRepository.GetDefault<MediaData>(
                        currentStorageFolder ?? SiteDefinition.Current.SiteAssetsRoot, contentType.ID);
                media.Name = string.Format("{0}", string.IsNullOrEmpty(name) ? filename : name);

             
                //Gets a blob (VPP object)
                Blob blob = blobFactory.CreateBlob(media.BinaryDataContainer, extension);

                //Write image stream to blob
                using (Stream inputStream = response.GetResponseStream())
                using (Stream outputStream = blob.OpenWrite())
                {
                    var buffer = new byte[4096];
                    int bytesRead;
                    do
                    {
                        bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                        outputStream.Write(buffer, 0, bytesRead);
                    } while (bytesRead != 0);
                }

                //Assign to file and publish changes
                media.BinaryData = blob;
                return _contentRepository.Save(media, SaveAction.Publish).ID.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Gets the URL of image by given identification. 
        /// </summary>
        /// <param name="idInStorage">The identifier in storage. ContentReference of the Media object</param>
        /// <returns>Url given by URL resolver for content reference</returns>
        public string GetUrl(string idInStorage)
        {
            int contentLinkId;
            if (!int.TryParse(idInStorage, out contentLinkId)) return string.Empty;

            var localCopy = new ContentReference(contentLinkId);
            try
            {
                return UrlResolver.Current.GetUrl(localCopy);
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the container for storage. ContentAssetFolder for given page
        /// </summary>
        /// <param name="storageContainerIdentyficator">The storage container identyficator.</param>
        /// <returns></returns>
        private ContentReference GetContainerForStorage(string storageContainerIdentyficator)
        {
            int pageId;
            if (int.TryParse(storageContainerIdentyficator, out pageId))
            {
                var content = _contentRepository.Get<PageData>(new ContentReference(pageId));
                if (content == null)
                    return null;

                if (content.ContentAssetsID == Guid.Empty)
                {
                    //Create asset folder for page if not exist.
                    var contentResourceFolder =
                        _contentRepository.GetDefault<ContentAssetFolder>(SiteDefinition.Current.ContentAssetsRoot);
                    content = content.CreateWritableClone();
                    contentResourceFolder.Attach(content);
                    _contentRepository.Save(contentResourceFolder, SaveAction.Publish);
                    _contentRepository.Save(content, SaveAction.Save);
                }
                return _contentRepository.Get<ContentAssetFolder>(content.ContentAssetsID).ContentLink;
            }
            return null;
        }


        private object GetFileName(string url)
        {
            return Path.GetFileName(@url);
        }

        private string GetFileExtension(string url)
        {
            return Path.GetExtension(@url);
        }
    }
}